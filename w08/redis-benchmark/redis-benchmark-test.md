## 1. 测试环境
- OS:    
  win11 WSL2 下 Ubuntu 20.04.3 LTS
- Kernel:    
  Linux  5.4.72-microsoft-standard-WSL2
- CPU:  
intel i5 11600K
- Disk: 
NVME/M2 SSD
- redis version:
6.2.6

## 2. 不同字节 value大小下，测试redis get性能
测试 10 20 50 100 200 1k 5k 字节 value 大小，redis get set 性能
### 2.1 命令及执行汇总
~~~bash 
./redis-benchmark -d 10 -t get
====== GET ======
  100000 requests completed in 0.63 seconds
  50 parallel clients
  10 bytes payload
  keep alive: 1
  host configuration "save": 3600 1 300 100 60 10000
  host configuration "appendonly": no
  multi-thread: no
Summary:
  throughput summary: 159235.66 requests per second
  latency summary (msec):
          avg       min       p50       p95       p99       max
        0.166     0.032     0.151     0.295     0.375     0.775
./redis-benchmark -d 20 -t get
====== GET ======
  100000 requests completed in 0.64 seconds
  50 parallel clients
  20 bytes payload
  keep alive: 1
  host configuration "save": 3600 1 300 100 60 10000
  host configuration "appendonly": no
  multi-thread: no
  Summary:
  throughput summary: 157232.70 requests per second
  latency summary (msec):
          avg       min       p50       p95       p99       max
        0.167     0.024     0.159     0.287     0.367     0.983
./redis-benchmark -d 50 -t get
====== GET ======
  100000 requests completed in 0.57 seconds
  50 parallel clients
  50 bytes payload
  keep alive: 1
  host configuration "save": 3600 1 300 100 60 10000
  host configuration "appendonly": no
  multi-thread: no
  Summary:
  throughput summary: 176056.33 requests per second
  latency summary (msec):
          avg       min       p50       p95       p99       max
        0.151     0.032     0.143     0.255     0.327     1.335
./redis-benchmark -d 100 -t get
====== GET ======
  100000 requests completed in 0.63 seconds
  50 parallel clients
  100 bytes payload
  keep alive: 1
  host configuration "save": 3600 1 300 100 60 10000
  host configuration "appendonly": no
  multi-thread: no
  Summary:
  throughput summary: 158478.61 requests per second
  latency summary (msec):
          avg       min       p50       p95       p99       max
        0.166     0.048     0.151     0.287     0.359     0.855
./redis-benchmark -d 200 -t get
====== GET ======
  100000 requests completed in 0.62 seconds
  50 parallel clients
  200 bytes payload
  keep alive: 1
  host configuration "save": 3600 1 300 100 60 10000
  host configuration "appendonly": no
  multi-thread: no
  Summary:
  throughput summary: 162601.62 requests per second
  latency summary (msec):
          avg       min       p50       p95       p99       max
        0.163     0.032     0.151     0.287     0.359     0.807
./redis-benchmark -d 1000 -t get
====== GET ======
  100000 requests completed in 0.52 seconds
  50 parallel clients
  1000 bytes payload
  keep alive: 1
  host configuration "save": 3600 1 300 100 60 10000
  host configuration "appendonly": no
  multi-thread: no
Summary:
  throughput summary: 190839.70 requests per second
  latency summary (msec):
          avg       min       p50       p95       p99       max
        0.155     0.032     0.135     0.271     0.367     0.903
./redis-benchmark -d 5000 -t get
====== GET ======
  100000 requests completed in 0.47 seconds
  50 parallel clients
  5000 bytes payload
  keep alive: 1
  host configuration "save": 3600 1 300 100 60 10000
  host configuration "appendonly": no
  multi-thread: no
  Summary:
  throughput summary: 213675.22 requests per second
  latency summary (msec):
          avg       min       p50       p95       p99       max
        0.145     0.032     0.135     0.255     0.311     1.151
~~~

### 2.2 总结:
随着value字节大小变大，redis get性能基本不变，始终在每秒15万-17万

## 3. 不同字节 value大小下，测试redis set性能

### 3.1 命令及执行汇总
~~~bash 
# 配置
  50 parallel clients
  keep alive: 1
  host configuration "save": 3600 1 300 100 60 10000
  host configuration "appendonly": no
  multi-thread: no
#
./redis-benchmark -d 10 -t set
  100000 requests completed in 0.67 seconds
  throughput summary: 149476.83 requests per second
  latency summary (msec):
          avg       min       p50       p95       p99       max
        0.177     0.040     0.159     0.295     0.407     1.071
./redis-benchmark -d 20 -t set
  100000 requests completed in 0.72 seconds
  20 bytes payload
  throughput summary: 139470.02 requests per second
  latency summary (msec):
          avg       min       p50       p95       p99       max
        0.187     0.016     0.175     0.303     0.383     0.743 
./redis-benchmark -d 50 -t set
  100000 requests completed in 0.60 seconds
  50 bytes payload
  throughput summary: 167224.08 requests per second
  latency summary (msec):
          avg       min       p50       p95       p99       max
        0.160     0.032     0.143     0.279     0.375     1.287  
./redis-benchmark -d 100 -t set
  100000 requests completed in 0.59 seconds
  100 bytes payload
  throughput summary: 170068.03 requests per second
  latency summary (msec):
          avg       min       p50       p95       p99       max
        0.157     0.032     0.151     0.279     0.335     0.991  
./redis-benchmark -d 200 -t set
  100000 requests completed in 0.66 seconds
  200 bytes payload
  throughput summary: 150602.42 requests per second
  latency summary (msec):
          avg       min       p50       p95       p99       max
        0.174     0.048     0.159     0.295     0.351     0.815  
./redis-benchmark -d 1000 -t set
  100000 requests completed in 0.58 seconds
  1000 bytes payload
  throughput summary: 171526.58 requests per second
  latency summary (msec):
          avg       min       p50       p95       p99       max
        0.153     0.032     0.143     0.255     0.311     0.863  
./redis-benchmark -d 5000 -t set
  100000 requests completed in 0.62 seconds
  5000 bytes payload
  throughput summary: 160513.64 requests per second
  latency summary (msec):
          avg       min       p50       p95       p99       max
        0.166     0.032     0.143     0.311     0.367     0.95  
~~~

### 3.2 总结:
随着value字节大小变大，redis set性能先升高后下降，从每秒14万到了近17万后降至16万