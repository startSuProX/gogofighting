package w02

import (
	"database/sql"
	"fmt"
	"github.com/pkg/errors"
)

var NotFound = errors.New("not found")

func Biz() error {
	err := Dao("")

	if errors.Is(err, NotFound) {
		//数据未找到，直接处理
		return nil
	}
	if err != nil {
		//说明是其他错误 向上抛出，
	}
	return nil
}

func Dao(query string) error {
	err := mockError()

	if err == sql.ErrNoRows {
		return errors.Wrapf(NotFound, fmt.Sprintf("data not found :%s", query))
	}
	if err != nil {
		return errors.Wrapf(err, fmt.Sprintf("db query error, error sql :%s", query))
	}
	return nil
}
